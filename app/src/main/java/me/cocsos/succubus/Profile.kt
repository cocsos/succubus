package me.cocsos.succubus

data class Profile(val name: String, val category: String, val concept: String,val feature:List<String>, val series: Int, var rawId: Int = 0, var drawableId: Int = 0, val name_en: String="", val raw_en: String="", var onPreview: Boolean = false, val open: Boolean = true)