package me.cocsos.succubus

import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.media.AudioManager
import android.media.MediaPlayer
import android.os.Bundle
import android.support.annotation.NonNull
import android.support.design.widget.BottomSheetDialog
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.GoogleAuthProvider
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.dialog_login.*
import me.cocsos.succubus.databinding.ActivityMainBinding
import me.cocsos.succubus.databinding.DialogLoginBinding
import me.cocsos.succubus.databinding.DialogStringBinding


class MainActivity : AppCompatActivity(), View.OnClickListener {


    private val TAG = this@MainActivity.javaClass.simpleName
    private val RC_SIGN_IN = 1000

    private var mediaPlayer: MediaPlayer? = null

    private val binding: ActivityMainBinding   by lazy {
        DataBindingUtil.setContentView<ActivityMainBinding>(this@MainActivity, R.layout.activity_main)
    }

    private var mGoogleSignInClient: GoogleSignInClient? = null
    private val mAuth: FirebaseAuth by lazy {
        FirebaseAuth.getInstance()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_main)
        Log.d(TAG, "onCreate: ")

        //백그라운드 초기화
        Glide.with(this@MainActivity)
                .load(R.drawable.bg_login)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(binding.ivBackground)

        //BGM 적용
        mediaPlayer = MediaPlayer.create(applicationContext, R.raw.bgm_succubus)
        mediaPlayer?.isLooping = true



        if (BaseApplication.sharedPreferences.getBoolean("bgm", true)) {
            mediaPlayer?.start()
        }

        popUpLogin()


    }


    private fun setLoginSetting() {

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build()


        mGoogleSignInClient = GoogleSignIn.getClient(this, gso)
    }

    override fun onDestroy() {
        super.onDestroy()
        try {
            when (mediaPlayer?.isPlaying) {
                true -> {
                    mediaPlayer?.stop()
                    mediaPlayer?.release()
                }
            }

        } catch (e: Exception) {
            Log.d(TAG, "onStop: ")
            e.printStackTrace()
        }
    }


    override fun onClick(v: View?) {
//        when(v?.id){
//            is R.id.
//        }
    }

//    public override fun onStart() {
//        super.onStart()
//        // Check if user is signed in (non-null) and update UI accordingly.
//        val currentUser = mAuth.currentUser
//        updateUI(currentUser)
//    }

    lateinit var dialog: BottomSheetDialog
    private fun popUpLogin() {

        Log.d(TAG, "popUpLogin: ")
        val bind = DataBindingUtil.inflate<DialogLoginBinding>(layoutInflater, R.layout.dialog_login, null, false);


        dialog = BottomSheetDialog(this@MainActivity)

        dialog.window?.requestFeature(Window.FEATURE_NO_TITLE)
        dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.setContentView(bind.root)
//        var width = resources.displayMetrics.widthPixels * 0.90
//        var height = resources.displayMetrics.heightPixels * 0.40


        val intent = Intent(applicationContext, BaseActivity::class.java)

        //대화창이 뜨면 0.5초 이후 인증정보를 가져오면서 맞으면 intent에 정보를 담고 media-> 완료 후 dismiss
        //아니면 가입 버튼을 보여주며 대기
        dialog.setOnShowListener {
            bind.pbLogin.animate()
                    .setDuration(100)
                    .withEndAction {
                        val currentUser = mAuth.currentUser
                        if (updateUI(currentUser)) {
                            Log.d(TAG, "popUpLogin: updateUi = true")
                            intent.putExtra("name", currentUser?.displayName ?: "방문객")
                            intent.putExtra("greeting", true)

                        } else {

                            Log.d(TAG, "popUpLogin: updateUi = false")
                        }
                    }
                    .start()
        }



        dialog.setOnDismissListener {
            bind.pbLogin.animate()
                    .setDuration(300)
                    .withEndAction {
                        Log.d(TAG, "popUpLogin: dismissing")
                        startActivity(intent)
                        finish()
                    }.start()
        }
        //게스트 버튼

        bind.tvGuest.setOnClickListener {

            intent.putExtra("name", "방문객")
            dialog.dismiss()
        }

        //로그인 버튼
        bind.googleLoginButton.setOnClickListener {
            setLoginSetting()
            signIn()
        }
        dialog.setCancelable(false)

        dialog.show()
    }

    private fun signIn() {
        val signInIntent = mGoogleSignInClient?.signInIntent
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                // Google Sign In was successful, authenticate with Firebase
                val account = task.getResult(ApiException::class.java)
                firebaseAuthWithGoogle(account)
            } catch (e: ApiException) {
                // Google Sign In failed, update UI appropriately
                Log.w(TAG, "Google sign in failed", e)
                Toast.makeText(this, "에러가 발생했습니다. -" + e.message, Toast.LENGTH_SHORT).show()

                dialog.pb_login.visibility = View.GONE
                dialog.cl_login.visibility = View.VISIBLE
            }

        }
    }


    private fun firebaseAuthWithGoogle(acct: GoogleSignInAccount?) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct?.id!!)

        val credential = GoogleAuthProvider.getCredential(acct.idToken, null)
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, object : OnCompleteListener<AuthResult> {
                    override fun onComplete(@NonNull task: Task<AuthResult>) {
                        if (task.isSuccessful) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success")
                            val user = mAuth.currentUser
                            updateUI(user)
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException())
                            currentFocus?.let { Snackbar.make(it, "Authentication Failed.", Snackbar.LENGTH_SHORT).show() }

                            dialog.pb_login.visibility = View.GONE
                            dialog.cl_login.visibility = View.VISIBLE
                            updateUI(null)
                        }

                    }
                })
    }


    private fun updateUI(user: FirebaseUser?): Boolean {

        if (user != null) {

            dialog.tv_name.text = "...\n네, 들어오세요"
            dialog.pb_login.visibility = View.GONE
            if (BaseApplication.sharedPreferences.getBoolean("bgm", true) && (getSystemService(Context.AUDIO_SERVICE) as AudioManager).isWiredHeadsetOn) {
                Log.d(TAG, "updateUI: user.displayName = ${user.displayName}")
                Log.d(TAG, "updateUI: user.email = ${user.email}")
                Log.d(TAG, "updateUI: sound playing")
                val mediaPlayer = MediaPlayer.create(this, R.raw.voice_laugh)
                mediaPlayer.start()
                mediaPlayer.setOnCompletionListener {
                    mediaPlayer.stop()
                    mediaPlayer.release()
                    Log.d(TAG, "setOnCompletionListener: sound end")
                    dialog.dismiss()
                }
                mediaPlayer.setOnErrorListener(object : MediaPlayer.OnErrorListener {
                    override fun onError(mp: MediaPlayer?, what: Int, extra: Int): Boolean {

                        mp?.reset()
//                        Log.d(TAG, "setOnErrorListener: sound error end")
//                        dialog.dismiss()
                        return true
                    }
                })
                return true
            } else {
                Log.d(TAG, "updateUI: no sound login")
                Log.d(TAG, "updateUI: user.displayName = ${user.displayName}")
                Log.d(TAG, "updateUI: user.email = ${user.email}")

                dialog.dismiss()
                return true
//                Toast.makeText(this, "${user?.displayName}", Toast.LENGTH_SHORT).show()
//                Toast.makeText(this, "인증 정보를 받아오지 못했습니다", Toast.LENGTH_SHORT).show()
            }


        } else {

            dialog.pb_login.visibility = View.GONE
            dialog.cl_login.visibility = View.VISIBLE
            return false
        }

    }
}
