package me.cocsos.succubus

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import android.support.v4.content.ContextCompat.startForegroundService
import android.os.Build



class AlarmReceiver : BroadcastReceiver() {

    val TAG = this@AlarmReceiver::class.java.simpleName
    val FROM_ALARM = "FROM_ALARM_RECEIVER"

    override fun onReceive(context: Context, intent: Intent) {

        Log.d(TAG, "onReceive: ")   //서비스를 실행한다

        val playServiceIntent = Intent(context, SoundPlayService::class.java)


        val keep = intent.getBooleanExtra("start", false)    //음악을 시작할지 결정
        val rawFile = intent.getIntExtra("rawFile", -1)     //어떤 음악인지

        playServiceIntent.putExtra("from", FROM_ALARM)
        playServiceIntent.putExtra("keep", keep)
        playServiceIntent.putExtra("rawFile", rawFile)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.startForegroundService(playServiceIntent)
        } else {
            context.startService(playServiceIntent)
        }
    }
}
