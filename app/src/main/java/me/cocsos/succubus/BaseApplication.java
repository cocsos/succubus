package me.cocsos.succubus;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.TypedValue;

import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;

import java.io.InputStream;

import io.fabric.sdk.android.Fabric;

public class BaseApplication extends Application {

    static Gson gson;

    String TAG = BaseApplication.class.getSimpleName();

    private static final String PREF_NAME = "Succubus";
    public static SharedPreferences sharedPreferences;

//    static public FirebaseAnalytics mFirebaseAnalytics;
//
    private static BaseApplication mInstance;

//    static RetrofitClient retrofitClient;


    public static InputStream getRaw(int id){
        return mInstance.getResources().openRawResource(id);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());

        // Set up Crashlytics, disabled for debug builds
//        Crashlytics crashlyticsKit = new Crashlytics.Builder()
//                .core(new CrashlyticsCore.Builder().disabled(com.crashlytics.android.BuildConfig.DEBUG).build())
//                .build();


        sharedPreferences = getSharedPreferences(PREF_NAME,MODE_PRIVATE);
        mInstance = this;
//
        gson = new Gson();

//        //페북 앱 이벤트 로거
//        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getApplicationContext());
//        logger = AppEventsLogger.newLogger(getApplicationContext());
//
//
//        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl(RetrofitClient.BASE_URL)
//                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
//                .addConverterFactory(ScalarsConverterFactory.create())
//                .addConverterFactory(GsonConverterFactory.create())
//                .build();
//
//         retrofitClient = retrofit.create(RetrofitClient.class);

    }


//
//    public static int dpToPixel(Context context, int dp) {
//        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics());
//    }

}
