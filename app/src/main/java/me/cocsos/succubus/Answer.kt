package me.cocsos.succubus

data class Answer(val sentence: String, var selected: Boolean=false)