package me.cocsos.succubus

import android.app.Dialog
import android.content.Context
import android.databinding.DataBindingUtil
import android.graphics.Color
import android.graphics.PorterDuff
import android.media.MediaPlayer
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.dialog_select_profile.*
import me.cocsos.succubus.databinding.ItemProfileBinding
import java.util.concurrent.TimeUnit

class ProfileAdapter constructor(private var context: Context, private var items: List<Profile>, private val cvm: ConversationViewModel, val dialog: Dialog) : RecyclerView.Adapter<ProfileAdapter.ProfileViewHolder>() {
    private val TAG = this@ProfileAdapter.javaClass.simpleName

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ProfileViewHolder {
        Log.d(TAG, "onCreateViewHolder: ")
        val bind = DataBindingUtil.inflate<ItemProfileBinding>(LayoutInflater.from(context), R.layout.item_profile, null, false)

        return ProfileViewHolder(bind, bind.root)
    }

    override fun getItemCount(): Int {
        return items.size
    }


    override fun onBindViewHolder(p0: ProfileViewHolder, p1: Int) {

        Log.d(TAG, "onBindViewHolder: " + items[p1])
        try {
            val binding = p0.binding
            binding.profile = items[p1]
            binding.rlContainer.setOnClickListener {
                dialog.dismiss()
                cvm.reservationCheck(items[p1],dialog)
//
//                if (sharedPreferences.getBoolean("isFirstReservation", true)) {
//                    sharedPreferences.edit().putBoolean("isFirstReservation", false).apply()
//                    cvm.letsTalkAbout("처음 예약한다")
//                } else {
//
                    cvm.letsTalkAbout("예약한다")
//                }
                Log.d(TAG, "onBindViewHolder: reservation " + items[p1].name)
            }

            binding.ivPlayer.setOnClickListener{

                preview(items[p1])

                notifyItemChanged(p1)
                notifyItemChanged(oldPosition)

                oldPosition = p1

            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }


    lateinit var previewPlayer: MediaPlayer
    var previewTime = 0
    var compositeDisposable: CompositeDisposable? = null
    var oldPosition = 0
    private fun preview(profile: Profile) {
//        (context.getSystemService(Context.AUDIO_SERVICE) as AudioManager).let {
//            if (!it.isWiredHeadsetOn) {
//                Toast.makeText(context, "이어폰을 착용해주세요", Toast.LENGTH_SHORT).show()
//                profile.onPreview = false
//                return
//            }
//        }

        //진행바 관리

        if (::previewPlayer.isInitialized && previewPlayer.isPlaying) { //현재 틀고있지만 다시 클릭해서
            if (items[oldPosition].onPreview == profile.onPreview) {  //지금 진행중인 프로필과 같다면
                previewTime = previewPlayer.currentPosition //현재 시간을 저장하고
                previewPlayer.pause()   //멈추기
                profile.onPreview = false    //끝
                return
            }
        } else if (::previewPlayer.isInitialized && !previewPlayer.isPlaying) { //현재 틀어져있지는 않지만 다시 클릭해서
            if (items[oldPosition].onPreview == profile.onPreview) {  //지금 진행중인 프로필과 같다면
                previewPlayer.seekTo(previewTime) //저장된 시간을 불러와서
                previewPlayer.start()   // 시작
                profile.onPreview = true   //끝
                return
            }
        }

        if (compositeDisposable == null) {
            compositeDisposable = CompositeDisposable()
        } else {
            compositeDisposable?.clear()
        }

        //새로 클릭한 경우.
        items[oldPosition].onPreview = false
        profile.onPreview = true

        previewPlayer = MediaPlayer.create(context, profile.rawId)
        previewPlayer.start()
        previewTime = 0

        dialog.tv_length.text = "/" + "%02d".format((previewPlayer.duration / 1000) / 60) + ":" + "%02d".format((previewPlayer.duration / 1000) % 60)
        dialog.tv_seekTo.text = "00:00"
        dialog.sb_preview.max = previewPlayer.duration / 1000
        dialog.sb_preview.progress = 0
        dialog.tv_seekTo.text = 0.toString()

        val disposable = Observable.range(0, previewPlayer.duration / 1000)
                .concatMap{ Observable.just(it).delay(1000, TimeUnit.MILLISECONDS) }  //1초마다 발행
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    dialog.sb_preview.progress = previewPlayer.currentPosition / 1000
                }, { Log.d(TAG, "seekbar: " + it.toString()) }, { Log.d(TAG, "talk: complete!") })
        compositeDisposable?.add(disposable)

        cvm.backgroundBgm(false)

        dialog.sb_preview.progressDrawable.setColorFilter(Color.RED, PorterDuff.Mode.SRC_IN)
        dialog.sb_preview.thumb.setColorFilter(Color.RED, PorterDuff.Mode.SRC_IN)
        dialog.sb_preview.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {

                dialog.tv_seekTo.text = "%02d".format(progress / 60) + ":" + "%02d".format(progress % 60)

                Log.d(TAG, "onProgressChanged: $progress")
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
                if (previewPlayer.isPlaying) {
                    previewPlayer.pause()
                }
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
                if (previewPlayer.isPlaying) {
                    previewPlayer.pause()
                }


                previewPlayer.seekTo(dialog.sb_preview.progress * 1000)
                previewTime = previewPlayer.currentPosition
                if (items[oldPosition].onPreview) {  //현재 듣고 있을때만
                    previewPlayer.start()
                }
            }
        })

        previewPlayer.setOnCompletionListener {  }

        dialog.setOnDismissListener {
            if (previewPlayer.isPlaying) {
                previewPlayer.pause()
            }

            items.map { it.onPreview=false }
            compositeDisposable?.clear()
            compositeDisposable = null
            cvm.backgroundBgm(true)
        }
    }


    inner class ProfileViewHolder(var binding: ItemProfileBinding, itemView: View) : RecyclerView.ViewHolder(itemView)

}