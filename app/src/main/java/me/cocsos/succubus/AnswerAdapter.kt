package me.cocsos.succubus

import android.app.Dialog
import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.util.ViewPreloadSizeProvider
import me.cocsos.succubus.databinding.ItemAnswerBinding

class AnswerAdapter constructor(private var context: Context, private var items: List<Answer>, private val cvm: ConversationViewModel, val dialog: Dialog) : RecyclerView.Adapter<AnswerAdapter.AnswerViewHolder>() {

    private val TAG = this@AnswerAdapter.javaClass.simpleName
    var oldPosition = 0
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AnswerViewHolder {
        Log.d(TAG, "onCreateViewHolder: ")
        var bind = DataBindingUtil.inflate<ItemAnswerBinding>(LayoutInflater.from(context), R.layout.item_answer, null, false)

        Glide.with(context)
                .load(R.drawable.ic_seleted)
                .asGif()
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(bind.ivSelected)

        bind.ivSelected.visibility=View.INVISIBLE

        return AnswerViewHolder(bind, bind.root)
    }

    override fun getItemCount(): Int {
        return items.size
    }


    override fun onBindViewHolder(p0: AnswerViewHolder, p1: Int) {

        Log.d(TAG, "onBindViewHolder: " + items[p1])
        try {
            var binding = p0.binding
            binding.answer = items[p1]
            binding.llAnswer.setOnClickListener {
                if (items[p1].selected) {
                    dialog.dismiss()
                    cvm.letsTalkAbout(items[p1].sentence)
                    Log.d("AnswerAdapter", "ask: " + items[p1].sentence)
                } else {
                    Log.d("AnswerAdapter", "select: " + items[p1].sentence)
                    items[oldPosition].selected = false
                    items[p1].selected = true

                    this.notifyItemChanged(oldPosition)
                    this.notifyItemChanged(p1)

                    oldPosition = p1
//                    this.notifyDataSetChanged()
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }


    inner class AnswerViewHolder(var binding: ItemAnswerBinding, itemView: View) : RecyclerView.ViewHolder(itemView)

}