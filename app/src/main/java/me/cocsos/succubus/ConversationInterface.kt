package me.cocsos.succubus

import android.app.Dialog

interface ConversationInterface {


    fun talk(fullText: String)  //전송된 text를 하나하나 나열한다. + typing 소리

//    fun completeText(fullText: String )  //텍스트를 한번에 보여준다

    fun popUpAnswer()  //선택지를 띄운다

    fun takeAction(subject: String) //선택지에 맞는 행동을 취하자.


    fun reservationYesno(profile: Profile,dialog: Dialog) //서큐버스 예약할까 말까
    fun popUpSelectProfile(concept:String) //서큐버스 예약할까 말까

    fun backgroundBgm(onOff: Boolean) //배경음악 켜고 끄기
    fun emotion(emotion: String) //배경음악 켜고 끄기


    fun showString(str: String) //문자열 하나를 bottomsheet으로 띄움


}