package me.cocsos.succubus

import android.app.*
import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.ColorDrawable
import android.media.MediaPlayer
import android.media.RingtoneManager
import android.os.Build
import android.os.Bundle
import android.support.design.widget.BottomSheetDialog
import android.support.design.widget.CoordinatorLayout
import android.support.v4.app.NotificationCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.SeekBar
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_base.*
import kotlinx.android.synthetic.main.dialog_image.*
import kotlinx.android.synthetic.main.view_conversation.*
import me.cocsos.succubus.BaseApplication.sharedPreferences
import me.cocsos.succubus.databinding.*
import java.util.*
import java.util.concurrent.TimeUnit


class BaseActivity : AppCompatActivity(), ConversationInterface, View.OnClickListener {


    private val TAG = this@BaseActivity.javaClass.simpleName

    private var detectSnoringService: Intent? = null
    private final val PERMISSION_CHECK = 1000

    private val binding by lazy {
        DataBindingUtil.setContentView<ActivityBaseBinding>(this@BaseActivity, R.layout.activity_base)
    }
    private val cvm: ConversationViewModel = ConversationViewModel(this)
    private val backgroundPlayer by lazy {
        //배경 음악
        MediaPlayer.create(this, R.raw.window_demons)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_base)

        Log.d(TAG, "onCreate _ ")

        binding.cvm = cvm

        //bgm 설정
        if (sharedPreferences.getBoolean("bgm", true)) {
            backgroundPlayer.start()
        }


        //이미지, 리스너 적용
        setImageAndListenser()
        //처음이면 intro 불러오기


        val feedback = sharedPreferences.getBoolean("succubusWork", false)
        val isFirst = sharedPreferences.getBoolean("isFisrt", true)
        val greeting = intent.getBooleanExtra("greeting", false)

        if (feedback) {
            cvm.letsTalkAbout("피드백")
            sharedPreferences.edit().putBoolean("succubusWork", false).apply()
        } else {
            if (isFirst) {
                cvm.letsTalkAbout("intro")


                sharedPreferences.edit().putBoolean("isFisrt", false).apply()
            } else {
                if (greeting) {
                    cvm.letsTalkAbout("welcomeAgain")
                } else {
                    cvm.letsTalkAbout("guestAgain")
                }
            }
        }
//        when (feedback) {
//            true -> {
//            }
//            false -> {
//                when (isFirst) {
//                    true -> {
//                    }
//                    false ->
//                        when (greeting) {
//                            true -> cvm.letsTalkAbout("welcomeAgain")
//                            false -> cvm.letsTalkAbout("guestAgain")
//                        }
//                }
//            }
//        }
    }

    private fun setImageAndListenser() {

        //백그라운드 초기화
        Glide.with(this@BaseActivity)
                .load(R.drawable.bg_base)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(binding.ivBackground)
        Glide.with(this@BaseActivity)
                .load(R.drawable.img_tiktok)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(iv_tiktok)

        iv_setting.setOnClickListener(this)

        binding.ivBackground.setOnClickListener(this)
        binding.viewClick.setOnClickListener(this)

//        R.id.binding.ivBackground -> cvm.nextSentence()
//        binding.layoutConversation.clConversationBackground
//        cl_conversation_background.dispatchTouchEvent(})
//    cl_conversation_background.onInterceptTouchEvent()
    }

    companion object {
        var wait = false
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.iv_background -> cvm.nextSentence()
            R.id.iv_setting -> onSettingButtonClicked()
            R.id.view_click -> cvm.nextSentence()

//            R.id.tv_stop -> onStopButtonClick()
        }
    }


    private var bgmTime: Int = 0
    override fun onPause() {
        super.onPause()
        backgroundBgm(false)
    }

    override fun onRestart() {
        super.onRestart()
        backgroundBgm(true)
    }

    override fun backgroundBgm(onOff: Boolean) {

        if (sharedPreferences.getBoolean("bgm", true)) {
            if (onOff) {
                backgroundPlayer.seekTo(bgmTime)
                backgroundPlayer.start()
            } else {

                backgroundPlayer.pause()
                bgmTime = backgroundPlayer.currentPosition
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        if (backgroundPlayer.isPlaying) {
            backgroundPlayer.stop()
            backgroundPlayer.release()
        }
        if (typingPlayer?.isPlaying == true) {
            typingPlayer?.stop()
            typingPlayer?.release()
        }
    }

    private fun onSettingButtonClicked() {
        Log.d(TAG, "onSettingButtonClicked: ")
        val bind = DataBindingUtil.inflate<DialogOptionBinding>(layoutInflater, R.layout.dialog_option, null, false)

        val dialog = Dialog(this@BaseActivity)

        dialog.window?.requestFeature(Window.FEATURE_NO_TITLE)
        dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)

        dialog.setContentView(bind.root)


        bind.swBgm.isChecked = sharedPreferences.getBoolean("bgm", true)
        bind.swNotification.isChecked = sharedPreferences.getBoolean("notification", true)
//        bind.sbDuration.


//ㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡ 수면까지 드는 시간 설정
        bind.sbSleep.max = 90

        bind.sbSleep.progress = sharedPreferences.getInt("sleep", 30)
        bind.sbSleep.progressDrawable.setColorFilter(Color.RED, PorterDuff.Mode.SRC_IN)
        bind.sbSleep.thumb.setColorFilter(Color.RED, PorterDuff.Mode.SRC_IN)


        bind.tvSleep.text = sharedPreferences.getInt("sleep", 30).toString()
        Log.d(TAG, "onSettingButtonClicked:tvSleep " + sharedPreferences.getInt("sleep", 30).toString())

        bind.sbSleep.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                bind.tvSleep.text = progress.toString()
                Log.d(TAG, "onProgressChanged:sbSleep $progress")
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
            }
        })
//ㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡ 램수면까지 필요한 시간 설정
        bind.sbRem.max = 180

        bind.sbRem.progress = sharedPreferences.getInt("rem", 90)
        bind.sbRem.progressDrawable.setColorFilter(Color.RED, PorterDuff.Mode.SRC_IN)
        bind.sbRem.thumb.setColorFilter(Color.RED, PorterDuff.Mode.SRC_IN)


        bind.tvRem.text = sharedPreferences.getInt("rem", 90).toString()
        Log.d(TAG, "onSettingButtonClicked:tvRem " + sharedPreferences.getInt("rem", 90).toString())

        bind.sbRem.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                bind.tvRem.text = progress.toString()
                Log.d(TAG, "onProgressChanged:sbRem $progress")
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
            }
        })
        //ㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡ 방문 기간 설정
        bind.sbDuration.max = 60

        bind.sbDuration.progress = sharedPreferences.getInt("duration", 10)
        bind.sbDuration.progressDrawable.setColorFilter(Color.RED, PorterDuff.Mode.SRC_IN)
        bind.sbDuration.thumb.setColorFilter(Color.RED, PorterDuff.Mode.SRC_IN)


        bind.tvDuration.text = sharedPreferences.getInt("duration", 10).toString()

        Log.d(TAG, "onSettingButtonClicked:tvDuration " + sharedPreferences.getInt("duration", 10).toString())

        bind.sbDuration.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                bind.tvDuration.text = progress.toString()
                Log.d(TAG, "onProgressChanged:tvDuration $progress")
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
            }
        })

//ㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡㅡ
        bind.tvConfirm.setOnClickListener {

            val editor = sharedPreferences.edit()
            editor.putBoolean("bgm", bind.swBgm.isChecked)
            if (bind.swBgm.isChecked) {
                if (!backgroundPlayer.isPlaying) {
                    backgroundPlayer.seekTo(0)
                    backgroundPlayer.start()
                }
            } else {
                if (backgroundPlayer.isPlaying) backgroundPlayer.pause()
            }
            editor.putBoolean("notification", bind.swNotification.isChecked)
            editor.putInt("duration", Integer.valueOf(bind.tvDuration.text.toString()))
            editor.putInt("sleep", Integer.valueOf(bind.tvSleep.text.toString()))
            editor.putInt("rem", Integer.valueOf(bind.tvRem.text.toString()))

            editor.apply()

            dialog.dismiss()
        }

        dialog.setCancelable(true)

        dialog.setOnDismissListener {
            talk("고객님은 " + bind.tvSleep.text.toString() + "분 뒤 주무시고,\n꿈을 꿀 때 까지 " + bind.tvRem.text.toString() + "분이 걸리므로\n"
                    + "서큐버스는 예약 후 " + (Integer.valueOf(bind.tvSleep.text.toString()) + Integer.valueOf(bind.tvRem.text.toString())) + "분 뒤\n"
                    + bind.tvDuration.text.toString() + "분 동안 방문합니다.")
        }
        val width = resources.displayMetrics.widthPixels * 0.90
        val height = resources.displayMetrics.heightPixels * 0.60
        dialog.window?.setBackgroundDrawableResource(R.color.transparent)
        dialog.window?.setLayout(width.toInt(), height.toInt())

        dialog.show()
    }

//    private fun initPermission(): Boolean {
//        val permission1 = ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
//        val permission2 = ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO)
//
//
//        if (permission1 != PackageManager.PERMISSION_GRANTED || permission2 != PackageManager.PERMISSION_GRANTED) {
//            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO), PERMISSION_CHECK)
//        } else {
//
//            return true
//        }
//        return false
//    }

//    private var recording = false
    //    private fun onRecordButtonClick() {
//
//        if (!initPermission()) return
//        if (!recording) {
//            tv_start.text = "시작되었습니다...오늘밤ㅎ"
//            recording = true
//            detectSnoringService = Intent(applicationContext, DetectSnoringService::class.java)
////            detectSnoringService = DetectSnoringService()
////            detectSnoringService?.startRecorder()?.run()
//            startService(detectSnoringService)
//        } else {
//            tv_start.text = "녹음 종료!"
//            recording = false
//            applicationContext.stopService(detectSnoringService)
//        }
//    }

    private val AlarmRequestCode = 1111


    private var alarmManager: AlarmManager? = null

    private fun reservation(profile: Profile) {
        if (!sharedPreferences.getBoolean("reserved", false)) {

            val calendar = Calendar.getInstance()

            calendar.add(Calendar.MINUTE, sharedPreferences.getInt("sleep", 30))
            calendar.add(Calendar.MINUTE, sharedPreferences.getInt("rem", 30))

//            val alarmIntent = Intent("me.cocsos.alarmfilter.ALARM_START")
            val alarmIntent = Intent(this, AlarmReceiver::class.java)

            alarmIntent.putExtra("start", true)    //음악을 시작할지 결정
            alarmIntent.putExtra("rawFile", profile.rawId)     //어떤 음악인지



            val pendingIntent = PendingIntent.getBroadcast(this, AlarmRequestCode, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT)
            alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager

            alarmManager?.set(AlarmManager.RTC_WAKEUP, calendar.timeInMillis, pendingIntent)

            showNotification()
            Log.d(TAG, "onRecordButtonClick: ALARM 설정됨")

            sharedPreferences.edit().putBoolean("reserved", true).apply()

        } else {          //방문 예약이 되어있는 경우
            Toast.makeText(this, "이미 예약되었습니다.", Toast.LENGTH_SHORT).show()
        }
    }

    private fun cancelReservation() {
        try {

            val isRunning = isServiceRunning(SoundPlayService::class.java)
            // 1. 아직 서큐버스가 오지 않았을 때
            Log.d(TAG, "onRecordButtonClick: isRunning? $isRunning")
            if (!isRunning) {
                val intent = Intent(this, AlarmReceiver::class.java)
                val pIntent = PendingIntent.getBroadcast(this, AlarmRequestCode, intent, PendingIntent.FLAG_CANCEL_CURRENT)

                alarmManager?.cancel(pIntent)
                Log.d(TAG, "onRecordButtonClick: call cancel")
            } else {
                stopService(Intent(this@BaseActivity, SoundPlayService::class.java))
                Log.d(TAG, "onRecordButtonClick: stop services")
            }
            cancelNotification()


//            Toast.makeText(this, "취소했습니다", Toast.LENGTH_SHORT).show()

            sharedPreferences.edit().putBoolean("reserved", false).apply()


            val size = cvm.answers.size
            for (i in 0 until size) {
                if (cvm.answers.get(i).equals("예약을 취소한다")) {
                    cvm.answers.set(i, "서큐버스를 예약한다")
                }
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

//    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
//        when (requestCode) {
//            PERMISSION_CHECK -> onRecordButtonClick()
//        }
//    }


    @SuppressWarnings("deprecation")
    private fun isServiceRunning(serviceClass: Class<out Any>): Boolean {
        val manager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        for (service in manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.name.equals(service.service.className)) {
                return true
            }
        }
        return false
    }

    private fun showNotification() {
        if (!sharedPreferences.getBoolean("notification", true)) return

        val titleIntent = Intent(this, BaseActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(this, 1, titleIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        val notifyID = 1
        val CHANNEL_ID = "me.cocsos.succubus.soundplaychannel"
        val now = Calendar.getInstance()
        now.add(Calendar.MINUTE, sharedPreferences.getInt("sleep", 30))
        now.add(Calendar.MINUTE, sharedPreferences.getInt("rem", 90))
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {   //오레오 이상일 경우
            Log.d(TAG, "sendNotification:  version >= 26 O")

            val b = Notification.Builder(this, CHANNEL_ID)
                    .setSmallIcon(R.drawable.ic_icon)
                    .setContentTitle("서큐버스 대기중")
//                    .setStyle( NotificationCompat.BigTextStyle().bigText("고객님이 잘 때까지 대기중입니다\n예상 방문 시간:%d시 %d분".format(now.get(Calendar.HOUR_OF_DAY), now.get(Calendar.MINUTE))))
                    .setContentText("고객님이 잘 때까지 대기중입니다. 예상 방문 시간:%d시 %d분".format(now.get(Calendar.HOUR_OF_DAY), now.get(Calendar.MINUTE)))
                    .setChannelId(CHANNEL_ID)
                    .setContentIntent(pendingIntent)

            val no = b.build()
            val importance = NotificationManager.IMPORTANCE_HIGH

            // Create a notification and set the notification channel.
            val mNotificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            val channel = NotificationChannel(CHANNEL_ID, "succubus_notification", importance)

            mNotificationManager.createNotificationChannel(channel)
            mNotificationManager.notify(notifyID, no)

        } else {
            Log.d(TAG, "sendNotification:  version < 26 O")
            val uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)

            val b = NotificationCompat.Builder(this, CHANNEL_ID)
                    .setSmallIcon(R.drawable.ic_icon)//.setLargeIcon(BitmapFactory.decodeResource(getResources(),R.mipmap.ic_launcher) )
                    .setContentTitle("서큐버스 대기중")
                    .setContentText("고객님이 잘 때까지 대기중입니다. 예상 방문 시간 : %02d시 %02d분".format(now.get(Calendar.HOUR_OF_DAY), now.get(Calendar.MINUTE)))
                    .setChannelId(CHANNEL_ID)
                    .setContentIntent(pendingIntent)
                    .setSound(uri)
                    .setVibrate(longArrayOf(0L))

            val no = b.build()
            val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

            notificationManager.notify(1 /* ID of notification */, no)
        }
    }

    private fun cancelNotification() {
        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.cancel(1)
    }


    private var typingPlayer: MediaPlayer? = null


    var compositeDisposable = CompositeDisposable()
    override fun talk(fullText: String) {
        tv_sentence.text = ""
        iv_tiktok.visibility = View.GONE

        if (typingPlayer == null) {
            typingPlayer = MediaPlayer.create(this, R.raw.bgm_typing)
            if (sharedPreferences.getBoolean("bgm", true)) {
                typingPlayer?.start()
            }
        } else if (typingPlayer?.isPlaying == false) {
            if (sharedPreferences.getBoolean("bgm", true)) {

                typingPlayer?.seekTo(0)
                typingPlayer?.start()
            }
        }
        try {
            compositeDisposable.clear()

            val disposable = Observable.range(0, fullText.length)
                    .concatMap({ Observable.just(it).delay(30, TimeUnit.MILLISECONDS) })
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ i ->
                        Log.d(TAG, "talk: " + fullText[i])

                        tv_sentence.text = tv_sentence.text.toString() + fullText[i]
                        if (i == fullText.length - 1) {

                            wait = false
                            iv_tiktok.visibility = View.VISIBLE
                            if (typingPlayer?.isPlaying == true) {
                                typingPlayer?.pause()
                                Log.d(TAG, "talk: player paused")
                            } else {
                                Log.d(TAG, "talk: player not running!")
                            }

                        }
                    }, { Log.d(TAG, "talk: " + it.toString()) }, { Log.d(TAG, "talk: complete! $fullText") })

            compositeDisposable.add(disposable)
        } catch (e: Exception) {
            e.printStackTrace()
            Log.d(TAG, "talk: ")
        }
    }


    override fun onBackPressed() {
        cvm.letsTalkAbout("나간다")
    }



   private  var byebye = false
    private fun byebye() {

        if(byebye) return
        val bind = DataBindingUtil.inflate<DialogYesnoBinding>(layoutInflater, R.layout.dialog_yesno, null, false)

        Log.d(TAG, "askFinish: ")

        val dialog = Dialog(this@BaseActivity)

        dialog.window?.requestFeature(Window.FEATURE_NO_TITLE)
        dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)

        dialog.setContentView(bind.getRoot())
        val width = resources.displayMetrics.widthPixels * 0.90
        val height = resources.displayMetrics.heightPixels * 0.25

        bind.tvCancel.setOnClickListener {
            dialog.dismiss()
        }
        bind.tvConfirm.setOnClickListener {
            emotion("바이바이")
            cvm.letsTalkAbout("바이바이")
            dialog.dismiss()
            binding.layoutConversation.tvSentence.animate()
                    .setDuration(1200)
                    .withEndAction {
                        finish()
                    }
                    .start()
            byebye = true
        }

        dialog.setCancelable(true)
        dialog.window?.setBackgroundDrawableResource(R.color.transparent)
        dialog.window?.setLayout(width.toInt(), height.toInt())

        binding.layoutConversation.tvSentence.animate()
                .setDuration(300)
                .withEndAction {
                    dialog.show()
                }
                .start()
    }


    override fun popUpAnswer() {

        if(byebye) return

        val bind = DataBindingUtil.inflate<DialogAnswerBinding>(layoutInflater, R.layout.dialog_answer, null, false)


        Log.d(TAG, "popUpAnswer: ")

        val dialog = Dialog(this@BaseActivity)

        dialog.window?.requestFeature(Window.FEATURE_NO_TITLE)
        dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)

        dialog.setContentView(bind.getRoot())
        val width = resources.displayMetrics.widthPixels * 0.90
        val height = resources.displayMetrics.heightPixels * 0.40


        var answers: List<Answer> = cvm.answers.map { Answer(it, false) }


        answers.map { Log.d(TAG, "popUpAnswer: $it") }
        answers[0].selected = true


        val layoutManager = LinearLayoutManager(this)
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        bind.rvAnswers.layoutManager = layoutManager
        bind.rvAnswers.adapter = AnswerAdapter(this, answers, cvm, dialog)
        bind.rvAnswers.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        bind.rvAnswers.setHasFixedSize(true)
        bind.rvAnswers.adapter?.notifyDataSetChanged()

        dialog.setCancelable(true)
        dialog.window?.setBackgroundDrawableResource(R.color.transparent)
        dialog.window?.setLayout(width.toInt(), height.toInt())

        dialog.show()

//        dialogOpen = true
    }

    lateinit var categoryViewModel: CategoryViewModel
    private fun popUpSelectCategory() {

        if (!::categoryViewModel.isInitialized) {
            categoryViewModel = CategoryViewModel()
            categoryViewModel.loadJson(this)
        }

        val bind = DataBindingUtil.inflate<DialogSelectCategoryBinding>(layoutInflater, R.layout.dialog_select_category, null, false)


        Log.d(TAG, "makeReservation: ")
        val dialog = Dialog(this@BaseActivity)

        dialog.window?.requestFeature(Window.FEATURE_NO_TITLE)

        dialog.setContentView(bind.root)
        val width = resources.displayMetrics.widthPixels * 0.90
        val height = resources.displayMetrics.heightPixels * 0.65


        val p = categoryViewModel.categories
//                ?: listOf(Category("ERROR", "", ""))


        Log.d(TAG, "popUpSelectCategory: $p")

        val layoutManager = GridLayoutManager(this, 2)
        bind.rvProfiles.layoutManager = layoutManager
        bind.rvProfiles.adapter = CategoryAdapter(this, p, cvm, dialog)


        bind.rvProfiles.adapter?.notifyDataSetChanged()

        dialog.setCancelable(true)
        dialog.window?.setBackgroundDrawableResource(R.color.transparent)
        dialog.window?.setLayout(width.toInt(), height.toInt())

//        dialog.show()

        binding.layoutConversation.tvSentence.animate()
                .setDuration(400)
                .withEndAction {
                    dialog.show()
                }
                .start()
    }


    lateinit var profileViewModel: ProfileViewModel


    override fun popUpSelectProfile(concept: String) {

        if (!::profileViewModel.isInitialized) {
            profileViewModel = ProfileViewModel()
        }
        profileViewModel.loadJson(this, concept)

        val bind = DataBindingUtil.inflate<DialogSelectProfileBinding>(layoutInflater, R.layout.dialog_select_profile, null, false)


        Log.d(TAG, "popUpSelectProfile: ")

        val dialog = Dialog(this@BaseActivity)

        dialog.window?.requestFeature(Window.FEATURE_NO_TITLE)
        dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)

        dialog.setContentView(bind.root)
        val width = resources.displayMetrics.widthPixels * 0.90
        val height = resources.displayMetrics.heightPixels * 0.65

        val p = profileViewModel.profiles

        Log.d(TAG, "popUpSelectCategory: $p")

        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        bind.rvProfiles.layoutManager = layoutManager
        bind.rvProfiles.adapter = ProfileAdapter(this, p, cvm, dialog)


        bind.rvProfiles.adapter?.notifyDataSetChanged()

        dialog.setCancelable(true)
        dialog.window?.setBackgroundDrawableResource(R.color.transparent)
        dialog.window?.setLayout(width.toInt(), height.toInt())

//        dialog.show()

        binding.layoutConversation.tvSentence.animate()
                .setDuration(400)
                .withEndAction {
                    dialog.show()
                }
                .start()
    }

    private fun showImage(id: Int) {

        val bind = DataBindingUtil.inflate<DialogImageBinding>(layoutInflater, R.layout.dialog_image, null, false)


        Log.d(TAG, "makeReservation: ")

        val dialog = Dialog(this@BaseActivity)

        dialog.window?.requestFeature(Window.FEATURE_NO_TITLE)
        dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)

        dialog.setContentView(bind.root)

        dialog.iv_image.setImageResource(id)

        dialog.setCancelable(true)
        dialog.window?.setBackgroundDrawableResource(R.color.transparent)
//        dialog.window?.setLayout(width.toInt(), height.toInt())

//        dialog.show()

        val width = resources.displayMetrics.widthPixels * 0.90F
        val height = width * 32.0F / 40.0F
        binding.layoutConversation.tvSentence.animate()
                .setDuration(1000)
                .withEndAction {
                    dialog.show()
                }
                .start()


        dialog.window?.setLayout(width.toInt(), height.toInt())

    }

    override fun takeAction(subject: String) {
        when (subject) {
            "나간다" -> byebye()
            "궁금한 점이 있는데..." -> cvm.getAnswers("questions")
            "돌아간다" -> cvm.getAnswers()
            "서큐버스를 예약한다" -> popUpSelectCategory()
            "예약을 취소한다" -> cancelReservation()
            "서큐버스는 어떻게 찾아오나요?" -> showImage(R.drawable.rem_denoise)
        }
    }


    override fun reservationYesno(profile: Profile, dialog: Dialog) {
//
//        if (dialogOpen) {
//            Log.d(TAG, "reservationYesno: dialog already opened")
//            return
//        }
        if (TextUtils.equals(profile.name, "ERROR")) {
            cvm.letsTalkAbout("전산 오류")
            return
        }
        val bind = DataBindingUtil.inflate<DialogYesnoBinding>(layoutInflater, R.layout.dialog_yesno, null, false)

        Log.d(TAG, "reservationYesno: ")

        val yesnoDialog = Dialog(this@BaseActivity)

        yesnoDialog.window?.requestFeature(Window.FEATURE_NO_TITLE)
        yesnoDialog.window?.setBackgroundDrawableResource(android.R.color.transparent)

        yesnoDialog.setContentView(bind.root)
        val width = resources.displayMetrics.widthPixels * 0.90
        val height = resources.displayMetrics.heightPixels * 0.25

        bind.tvQuestion.text = "예약할까요?"
        bind.tvCancel.setOnClickListener {
            yesnoDialog.dismiss()
            dialog.show()
        }
        bind.tvConfirm.setOnClickListener {
            cvm.letsTalkAbout("예약 확인")

            for (i in 0 until cvm.answers.size) {
                if (TextUtils.equals(cvm.answers[i], "서큐버스를 예약한다")) {
                    cvm.answers[i] = "예약을 취소한다"
                }
            }

            reservation(profile)
            yesnoDialog.dismiss()
        }
        yesnoDialog.setCancelable(true)
        yesnoDialog.window?.setBackgroundDrawableResource(R.color.transparent)
        yesnoDialog.window?.setLayout(width.toInt(), height.toInt())

        binding.layoutConversation.tvSentence.animate()
                .setDuration(500)
                .withEndAction {
                    yesnoDialog.show()
                }
                .start()
//        dialogOpen = true
    }


    override fun emotion(emotion: String) {
        var drawable = 0
        when (emotion) {
            "바이바이" -> drawable = R.drawable.emotion_byebye
        }


        Glide.with(this@BaseActivity)
                .load(drawable)
                .asGif()
                .placeholder(R.drawable.img_placeholder)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(binding.ivBackground)
    }


    override fun showString(str: String) {
        val bind = DataBindingUtil.inflate<DialogStringBinding>(layoutInflater, R.layout.dialog_string, null, false)

        Log.d(TAG, "reservationYesno: ")

        val dialog = BottomSheetDialog(this@BaseActivity)

        dialog.window?.requestFeature(Window.FEATURE_NO_TITLE)
        dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)

        dialog.setContentView(bind.root)
        val width = resources.displayMetrics.widthPixels * 0.90
        val height = resources.displayMetrics.heightPixels * 0.25

        bind.sentence = str
        bind.tvSentence.setOnClickListener {
            dialog.dismiss()
        }
        dialog.setCancelable(true)
//        dialog.window?.setBackgroundDrawableResource(R.color.transparent)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window?.setLayout(width.toInt(), height.toInt())

//        var params = bind.root.layoutParams as CoordinatorLayout.LayoutParams
//
//        CoordinatorLayout.Behavior behavior = params.getBehavior();
//        ((View) contentView.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));
//                .setBackgroundColor(getResources().getColor(android.R.color.transparent));

        (bind.root.parent as View).setBackgroundResource(android.R.color.transparent)

        dialog.show()
    }
}
