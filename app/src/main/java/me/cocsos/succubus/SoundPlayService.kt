package me.cocsos.succubus

import android.app.*
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.media.AudioManager
import android.media.MediaPlayer
import android.media.RingtoneManager
import android.os.Build
import android.os.IBinder
import android.support.v4.app.NotificationCompat
import android.util.Log
import android.widget.Toast
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import me.cocsos.succubus.BaseApplication.sharedPreferences
import java.util.*
import java.util.concurrent.TimeUnit


class SoundPlayService : Service() {

    private val TAG = this@SoundPlayService::class.java.simpleName
    private var mediaPlayer: MediaPlayer? = null

    private val FROM_ALARM = "FROM_ALARM_RECEIVER"
    private val FROM_HEADPHONE = "FROM_HEADPHONE_RECEIVER"
    private var current: Int = 0

    //이어폰 감지 리시버 생성
    private val receiver: HeadphoneReceiver = HeadphoneReceiver()

    private val start: Calendar = Calendar.getInstance()
    private val end: Calendar = Calendar.getInstance()

    //방문 기간
    private val duration: Int = sharedPreferences.getInt("duration", 10)
    private val compositeDisposable = CompositeDisposable()

    init {
        //종료시간 설정
        end.add(Calendar.MINUTE, duration)
    }


    var inited = false
    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        val NOTIFICATION_ID = (System.currentTimeMillis() % 10000).toInt()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForeground(NOTIFICATION_ID, Notification.Builder(this).build())
        }


        //초기화된 적이 있다면
        if (!inited) {
            val receiverFilter = IntentFilter(Intent.ACTION_HEADSET_PLUG)
            registerReceiver(receiver, receiverFilter)
            inited = true
        }

        when (intent?.extras?.getString("from", null)) {

            FROM_ALARM -> {
                val keep = intent.extras?.getBoolean("keep", false)
                val rid: Int = intent.extras?.getInt("rawFile") ?: R.raw.bgm_typing

                when (keep) {
                    true -> playXXX(rid)
                    false -> stopXXX()
                }
            }
            FROM_HEADPHONE -> {
                val state = intent.extras?.getInt("state", 0)
                when (state) {
                    0 -> pauseXXX()
                    1 -> resumeXXX()
                }
            }
            else -> {
                Log.d(TAG, "onStartCommand:너는 어디서 왔니 $intent")
            }
        }


        return START_STICKY
    }

    private fun pauseXXX() {
        mediaPlayer?.pause()
        current = mediaPlayer?.currentPosition ?: 0
        Log.d(TAG, "pauseXXX: pause at $current")
        showNotification("이어폰을 착용해주세요", false)

    }

    private fun resumeXXX() {
        if (mediaPlayer?.isPlaying == true) {

        } else {

            mediaPlayer?.seekTo(current)
            mediaPlayer?.start()
            Log.d(TAG, "resumeXXX: resume at $current")

            showNotification("서큐버스가 방문했습니다. 예상 종료 시간 : %02d시 %02d분".format(end.get(Calendar.HOUR_OF_DAY), end.get(Calendar.MINUTE)), false)

        }
    }


    private fun playXXX(rid: Int) {

        val am1 = getSystemService(Context.AUDIO_SERVICE) as AudioManager

        Log.d(TAG, "onStartCommand on? = +" + am1.isWiredHeadsetOn)

        Log.d(TAG, "onStartCommand: rid = $rid")
        Toast.makeText(this, "서큐버스가 찾아왔습니다", Toast.LENGTH_SHORT).show()

        mediaPlayer = MediaPlayer.create(this, rid)
        mediaPlayer?.start()
        if (am1.isWiredHeadsetOn) {
            mediaPlayer?.start()
            startTimer()
            mediaPlayer?.setOnCompletionListener {
                //                this.stopXXX()
                if (end > Calendar.getInstance()) {
                    current = 0
                    mediaPlayer?.seekTo(0)
                    mediaPlayer?.start()
                } else {

                    sharedPreferences.edit().putBoolean("succubusWork", true).apply()
                    Log.d(TAG, "playXXX: 임무 달성!")
                    this.stopSelf()
                }
            }

            showNotification("서큐버스가 방문했습니다. 예상 종료 시간 : %02d시 %02d분".format(end.get(Calendar.HOUR_OF_DAY), end.get(Calendar.MINUTE)), false)

        } else {
            mediaPlayer?.pause()
            current = 0
//            Toast.makeText(this, "당신만 들으라고 했으텐데요...?", Toast.LENGTH_SHORT).show()
            showNotification("이어폰을 착용해주세요", true)
            Log.d(TAG, "playXXX: no headphone found... just Created")
        }
    }

    private fun stopXXX() {
        try {
            Toast.makeText(this, "서큐버스가 떠납니다", Toast.LENGTH_SHORT).show()
            val isPlaying = mediaPlayer?.isPlaying ?: false
            if (isPlaying) {
                mediaPlayer?.stop()
                mediaPlayer?.release()
            }
        } catch (e: Exception) {
            Log.d(TAG, "stopXXX: ")
            e.printStackTrace()
        }
    }

    private fun startTimer() {

        val msec = end.timeInMillis - start.timeInMillis

        Log.d(TAG, "startTimer: msec = $msec, 0~ ${msec.toInt()/100+1} 회 알림")
//
//        val disposable = Observable.range(0, (1 + msec/1000).toInt())   //차이나는 초+1 만큼
//                .concatMap { Observable.just(it).delay(1000, TimeUnit.MILLISECONDS) } //1초에 한번
        val disposable = Observable.range(0, msec.toInt()/100+1)   //차이나는 초+1 만큼
                .concatMap { Observable.just(it).delay(10, TimeUnit.SECONDS) } //1초에 한번
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Log.d(TAG, "startTimer: "+it*10)
                    if (end < Calendar.getInstance()) {
                        stopSelf()
                        Log.d(TAG, "startTimer: end< Calendar.getInstance() , stopSelf() called  ")
                    }else{
                        val s = (end.timeInMillis - Calendar.getInstance().timeInMillis)/1000
                        Log.d(TAG, "startTimer:  $s sec remain" )
                    }
                }, { Log.d(TAG, "startTimer: " + it.toString()) }, { Log.d(TAG, "startTimer: complete!  ") })
        compositeDisposable.add(disposable)

    }

    private fun showNotification(content: String, cancelable: Boolean) {

        if (!sharedPreferences.getBoolean("notification", true)) return

        val titleIntent = Intent(this, BaseActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(this, 1, titleIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        val notifyID = 1
        val CHANNEL_ID = "me.cocsos.succubus.soundplaychannel"
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {   //오레오 이상일 경우
            Log.d(TAG, "sendNotification:  version >= 26 O")

            val b = Notification.Builder(this, CHANNEL_ID)
                    .setSmallIcon(R.drawable.ic_icon)
                    .setContentTitle("서큐버스")
                    .setContentText(content)
                    .setChannelId(CHANNEL_ID)
                    .setOngoing(!cancelable)
                    .setContentIntent(pendingIntent)

            val no = b.build()
            val importance = NotificationManager.IMPORTANCE_HIGH

            // Create a notification and set the notification channel.
            val mNotificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            val channel = NotificationChannel(CHANNEL_ID, "succubus_notification", importance)

            mNotificationManager.createNotificationChannel(channel)
            mNotificationManager.notify(notifyID, no)

        } else {
            Log.d(TAG, "sendNotification:  version < 26 O")
            val uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)

            val b = NotificationCompat.Builder(this, CHANNEL_ID)
                    .setSmallIcon(R.drawable.ic_icon)
                    .setContentTitle("서큐버스")
                    .setContentText(content)
                    .setChannelId(CHANNEL_ID)
                    .setContentIntent(pendingIntent)
                    .setOngoing(!cancelable)
                    .setSound(uri)
                    .setVibrate(longArrayOf(0L))

            val no = b.build()
            val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

            notificationManager.notify(1 /* ID of notification */, no)
        }
    }


    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "onDestroy: ")
        stopXXX()
        showNotification("갈게요,바이바이!", true)

        BaseApplication.sharedPreferences.edit().putBoolean("reserved", false).apply()
        try {
            compositeDisposable.dispose()
            unregisterReceiver(receiver)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    override fun onBind(intent: Intent): IBinder? = null
}
