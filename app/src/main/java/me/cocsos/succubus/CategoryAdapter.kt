package me.cocsos.succubus

import android.app.Dialog
import android.content.Context
import android.databinding.DataBindingUtil
import android.media.MediaPlayer
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.reactivex.disposables.CompositeDisposable
import me.cocsos.succubus.databinding.ItemCategoryBinding
import me.cocsos.succubus.databinding.ItemProfileBinding

class CategoryAdapter constructor(private var context: Context, private var items: List<Category>, private val cvm: ConversationViewModel, val dialog: Dialog) : RecyclerView.Adapter<CategoryAdapter.ProfileViewHolder>() {
    private val TAG = this@CategoryAdapter.javaClass.simpleName


    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ProfileViewHolder {
        Log.d(TAG, "onCreateViewHolder: ")
        var bind = DataBindingUtil.inflate<ItemCategoryBinding>(LayoutInflater.from(context), R.layout.item_category, null, false)

        return ProfileViewHolder(bind, bind.root)
    }

    override fun getItemCount(): Int {
        return items.size
    }


    override fun onBindViewHolder(p0: ProfileViewHolder, p1: Int) {

        Log.d(TAG, "onBindViewHolder: " + items[p1])
        try {
            var binding = p0.binding
            binding.category = items[p1]
            binding.clProfile.setOnClickListener {
                dialog.dismiss()
                cvm.popUpSelectProfile(items[p1].name)
                Log.d(TAG, "onBindViewHolder: category " + items[p1].name)
            }

            binding.ivQuestion.setOnClickListener{
                cvm.showString(items[p1].description)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }
    inner class ProfileViewHolder(var binding: ItemCategoryBinding, itemView: View) : RecyclerView.ViewHolder(itemView)

}