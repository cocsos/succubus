package me.cocsos.succubus

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log

class HeadphoneReceiver : BroadcastReceiver() {

    val TAG = this@HeadphoneReceiver::class.java.simpleName
    val FROM_HEADPHONE = "FROM_HEADPHONE_RECEIVER"

    override fun onReceive(context: Context, intent: Intent) {

        Log.d(TAG, "onReceive: ")   //서비스를 실행한다
        if (BaseApplication.sharedPreferences.getBoolean("reserved", false)) {    //예약 되어있을경우에만 service에 전달

            val playServiceIntent = Intent(context, SoundPlayService::class.java)

            if (intent.action?.equals(Intent.ACTION_HEADSET_PLUG) == true) {    //만약 이어폰 상태가 변경돼서 온거라면
                val state = intent.getIntExtra("state", -1)

                Log.d(TAG, "onReceive: headphone state = " + state)
//            when (state) {
//                0 -> playServiceIntent.putExtra("state", state)
//                1 -> playServiceIntent.putExtra("state", state)
//              }

                playServiceIntent.putExtra("from", FROM_HEADPHONE)
                playServiceIntent.putExtra("state", state)

                context.startService(playServiceIntent)
            }

        } else {
            Log.d(TAG, "onReceive: not reserved")
        }
    }
}
