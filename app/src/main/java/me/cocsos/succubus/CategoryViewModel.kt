package me.cocsos.succubus

import android.content.Context
import android.databinding.BaseObservable
import android.databinding.Bindable
import android.databinding.BindingAdapter
import android.util.Log
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.google.gson.reflect.TypeToken
import java.io.BufferedReader
import kotlin.properties.Delegates


class CategoryViewModel : BaseObservable() {

    val TAG = this@CategoryViewModel.javaClass.simpleName

    //    @get:Bindable
    lateinit var categories: List<Category>
//            by Delegates.observable(listOf(Category("ERROR", "",""))) { _, _, _ ->
//                notifyPropertyChanged(BR.profiles)
//            }


    fun loadJson(context: Context) {
        try {
            val rawId = R.raw.categories
            val inputStream = BaseApplication.getRaw(rawId)
            Log.d(TAG, "loadJson: profilesId = $rawId")
            val read = inputStream.bufferedReader().use(BufferedReader::readText)
            inputStream.close()
            Log.d(TAG, "loadJson: profilesJsonString = $read ")

            val categories = BaseApplication.gson.fromJson<List<Category>>(read, object : TypeToken<List<Category>>() {}.type)
            val packName = context.packageName// 패키지명

            categories.map {
                it.drawableId = context.resources.getIdentifier("img_" + it.drawableName, "drawable", packName)
            }

            this.categories = categories

        } catch (e: Exception) {
            e.printStackTrace()
            Log.d(TAG, "loadJson: file read error")
            this.categories = listOf(Category("ERROR", "", ""))
        }
    }

    companion object {

        @BindingAdapter("drawableId")
        @JvmStatic
        fun setImage(view: ImageView, id: Int?) {
            Log.d("setProfileImage", "setImage: $id")
            Glide.with(view.context)
                    .load(id)
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .into(view)
        }
    }

}