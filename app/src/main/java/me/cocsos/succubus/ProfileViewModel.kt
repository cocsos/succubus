package me.cocsos.succubus

import android.content.Context
import android.databinding.BaseObservable
import android.databinding.Bindable
import android.databinding.BindingAdapter
import android.text.TextUtils
import android.util.Log
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.google.gson.reflect.TypeToken
import java.io.BufferedReader
import kotlin.properties.Delegates


class ProfileViewModel : BaseObservable() {

    val TAG = this@ProfileViewModel.javaClass.simpleName

    @get:Bindable
    lateinit var profiles: List<Profile>
    lateinit var totalProfiles: List<Profile>
//    listOf(Profile("ERROR", "","", listOf("?"), 0, R.raw.bgm_typing, R.drawable.img_missingno))


    fun loadJson(context: Context, str: String) {
        try {

            if(!::totalProfiles.isInitialized) {
                val rawId = R.raw.profiles
                val inputStream = BaseApplication.getRaw(rawId)
                Log.d(TAG, "loadJson: profilesId = $rawId")
                val read = inputStream.bufferedReader().use(BufferedReader::readText)
                inputStream.close()
                Log.d(TAG, "loadJson: profilesJsonString = $read ")

                totalProfiles = BaseApplication.gson.fromJson<List<Profile>>(read, object : TypeToken<List<Profile>>() {}.type)
                val packName = context.packageName// 패키지명

                totalProfiles.map {
                    it.drawableId = context.resources.getIdentifier("profile_" + it.name_en, "drawable", packName)
                    it.rawId = context.resources.getIdentifier(it.raw_en, "raw", packName)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Log.d(TAG, "loadJson: file read error")
            this.profiles = listOf(Profile("ERROR", "","", listOf("?"), 0, R.raw.bgm_typing, R.drawable.img_missingno))
        }



        this.profiles = totalProfiles.filter { TextUtils.equals(it.concept,str) }

    }

    companion object {
        @BindingAdapter("feature")
        @JvmStatic
        fun setFeature(view: TextView, list: List<String>?) {
            view.text = list.toString().substring(1, list.toString().length - 1)
        }
//
//        @BindingAdapter("profileId")
//        @JvmStatic
//        fun setProfileImage(view: ImageView, id: Int?) {
//            Log.d("setProfileImage", "setImage: $id")
//            Glide.with(view.context)
//                    .load(id)
//                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
//                    .into(view)
//        }
    }

}