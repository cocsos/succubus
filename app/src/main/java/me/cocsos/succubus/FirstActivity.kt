package me.cocsos.succubus

import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import me.cocsos.succubus.databinding.ActivityFirstBinding


class FirstActivity : AppCompatActivity()  {


    private val TAG = this@FirstActivity.javaClass.simpleName

    private val binding by lazy {
        DataBindingUtil.setContentView<ActivityFirstBinding>(this@FirstActivity, R.layout.activity_first)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d(TAG, "onCreate _ ")

        if (BaseApplication.sharedPreferences.getBoolean("agree", false)) {
            startActivity(Intent(applicationContext, MainActivity::class.java))
            finish()
        }
        //이미지, 리스너 적용
        setButtons()
        //처음이면 intro 불러오기
    }

    private fun setButtons() {
        binding.tvAgree.setOnClickListener {
            binding.btnAgree.isChecked = !binding.btnAgree.isChecked
            binding.btnAgree.setBackgroundResource(R.drawable.btn_checked)
            BaseApplication.sharedPreferences.edit().putBoolean("agree", true).apply()

            binding.btnAgree.animate()
                    .setDuration(300)
                    .withEndAction{

                        startActivity(Intent(applicationContext, MainActivity::class.java))
                        finish()
                    }.start()
        }
        binding.btnAgree.setOnClickListener {
            binding.btnAgree.isChecked = !binding.btnAgree.isChecked

            binding.btnAgree.setBackgroundResource(R.drawable.btn_checked)
            BaseApplication.sharedPreferences.edit().putBoolean("agree", true).apply()

            binding.btnAgree.animate()
                    .setDuration(300)
                    .withEndAction{

                        startActivity(Intent(applicationContext, MainActivity::class.java))
                        finish()
                    }.start()
        }
    }
}


