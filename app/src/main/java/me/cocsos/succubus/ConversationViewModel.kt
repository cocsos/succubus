package me.cocsos.succubus

import android.app.Dialog
import android.databinding.BaseObservable
import android.text.TextUtils
import android.util.Log
import android.widget.Toast
import com.google.gson.reflect.TypeToken
import me.cocsos.succubus.BaseApplication.sharedPreferences
import org.json.JSONObject
import java.io.BufferedReader


class ConversationViewModel(private val conversationInterface: ConversationInterface) : BaseObservable() {
    private val TAG = this@ConversationViewModel.javaClass.simpleName

    //기본 메뉴를 저장
    private var scripts: JSONObject


    init {
        scripts = loadJson()

        getAnswers()
    }


    private lateinit
    var sentenceList: Array<String>
    private var sequence = 0

    //다음 대화가 있으면 다음 대화로.
    //마지막 문장이면 answers로 된 다이얼로그 띄우기
    lateinit  var answers: Array<String>

    fun nextSentence() {

//        if (::sentenceList.isInitialized) {       초기화 검사.
//    }

//        if (wait) { //타자를 기다리지 않고 눌렀다면
//conversationInterface.completeText(sentenceList[sequence])
//            return
//        }
//        wait = true
        sentenceList.size.let {
            if (sequence + 1 == it) {

                conversationInterface.popUpAnswer()
                return
            }
        }
        sequence++
        conversationInterface.talk(sentenceList[sequence])


    }


    //도움말 혹은 초기 메뉴를 읽어온다
    fun getAnswers(menu: String = "menu") {
        if (scripts.has(menu)) {
            answers = BaseApplication.gson.fromJson<Array<String>>(scripts.getString(menu), object : TypeToken<Array<String>>() {}.type)


            if (sharedPreferences.getBoolean("reserved", false)) {
                var size = answers.size
                for (i in 0 until size) {
                    if (TextUtils.equals(answers[i],"서큐버스를 예약한다")) {
                        answers[i] ="예약을 취소한다"
                    }
                }
            }

        }else{
            Log.d(TAG, "getAnswers: ERROR NO SUCH MENU")
        }
    }


    //    var subject: String = ""
    //answer를 클릭했을때 주제 초기화
    fun letsTalkAbout(subject: String) {
        try {
            if (scripts.has(subject)) {

                conversationInterface.takeAction(subject)
                sequence = 0
                sentenceList = BaseApplication.gson.fromJson<Array<String>>(scripts.getString(subject), object : TypeToken<Array<String>>() {}.type)

            } else {
                sentenceList = arrayOf("무슨 말씀을 하시는지 잘 모르겠네요")

            }
            conversationInterface.talk(sentenceList[0])
        } catch (e: Exception) {
            Log.d(TAG, "letsTalkAboud: ")
            e.printStackTrace()
        }
    }


    //클릭한 서큐버스 예약할까요?
    fun reservationCheck(succubus: Profile,dialog: Dialog) {
        try {
            conversationInterface.reservationYesno(succubus,dialog)
        } catch (e: Exception) {
            Log.d(TAG, "reservationCheck: ")
            e.printStackTrace()
        }
    }

    private fun loadJson(): JSONObject {
        try {
            val scriptId = R.raw.scripts
            val inputStream = BaseApplication.getRaw(scriptId)
            Log.d(TAG, "loadJson: scriptId = $scriptId")
            val read = inputStream.bufferedReader().use(BufferedReader::readText)
            inputStream.close()
            Log.d(TAG, "loadJson: scriptJsonString = $read ")

            return JSONObject(read)
        } catch (e: Exception) {
            e.printStackTrace()
            Log.d(TAG, "loadJson: file read error")

            return JSONObject()
        }
    }



    fun backgroundBgm(boolean:Boolean){
        conversationInterface.backgroundBgm(boolean)
    }
    fun showString(str:String){
        conversationInterface.showString(str)
    }
    fun popUpSelectProfile(str:String){
        conversationInterface.popUpSelectProfile(str)
    }


}