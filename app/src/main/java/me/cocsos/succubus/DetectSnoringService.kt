package me.cocsos.succubus

import android.app.Service
import android.content.Intent
import android.media.AudioFormat
import android.media.AudioRecord
import android.media.AudioTrack
import android.media.MediaRecorder
import android.os.Environment
import android.os.IBinder
import android.util.Log
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import android.widget.Toast


class DetectSnoringService : Service() {

    private val mAudioSource = MediaRecorder.AudioSource.MIC
    private val TAG = this@DetectSnoringService.javaClass.simpleName
    private val RECORDER_SAMPLERATE = 8000
    private val RECORDER_CHANNELS = AudioFormat.CHANNEL_IN_MONO
    private val RECORDER_AUDIO_ENCODING = AudioFormat.ENCODING_PCM_16BIT
    private var mFilepath = ""
    private var mBufferSize = -1;

    private var mAudioRecord: AudioRecord

    init {
        mBufferSize = AudioTrack.getMinBufferSize(RECORDER_SAMPLERATE, RECORDER_CHANNELS, RECORDER_AUDIO_ENCODING)

        Log.d(TAG, "size =  $mBufferSize")

        if (mBufferSize < 0) mBufferSize = 1024;
//        if (mBufferSize > 0) {
        mAudioRecord = AudioRecord(mAudioSource, RECORDER_SAMPLERATE, RECORDER_CHANNELS, RECORDER_AUDIO_ENCODING, mBufferSize)
//        }
    }


    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        startRecorder().run()
        return super.onStartCommand(intent, flags, startId)
    }

    override fun onDestroy() {
        //서비스가 중지되면 녹음을 중지한다
//        Toast.makeText(this, "Record Service가 중지되었습니다.", Toast.LENGTH_LONG).show()
        Log.d(TAG, "Record Service가 중지되었습니다 onDestroy()")
        stopRecording();
    }


    private var isRecording = true
    fun startRecorder(): Runnable = Runnable {

        val readData = ByteArray(mBufferSize)

        mFilepath = Environment.getExternalStorageDirectory().absolutePath + "/record.pcm"
        var fos: FileOutputStream? = null
        try {
            fos = FileOutputStream(mFilepath)
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        }

        Log.d(TAG, "Record Service가 시작.")


        while (isRecording) {
            val ret = mAudioRecord.read(readData, 0, mBufferSize)  //  AudioRecord의 read 함수를 통해 pcm data 를 읽어옴
            Log.d(TAG, "read bytes is $ret")
            if (ret <= 0) {
                try {
                    mAudioRecord.startRecording() // restart audio recording
                } catch (e: Exception) {
                    Log.e(TAG, "exception", e) // Error trying to sleep thread
                }
            }
            Thread.sleep(100, 0)
            try {
                fos!!.write(readData, 0, mBufferSize)    //  읽어온 readData 를 파일에 write 함
            } catch (e: IOException) {
                e.printStackTrace()
            }

        }

        mAudioRecord.stop()
        mAudioRecord.release()

        try {
            fos!!.close()
        } catch (e: IOException) {
            e.printStackTrace()
        } finally {
            Log.d(TAG, "startRecorder: ended, saved => $mFilepath")
        }
    }

    fun stopRecording() {
        isRecording = false;
    }

    override fun onBind(intent: Intent): IBinder? = null
}
